from flask import (
    Flask,
    render_template,
    url_for,
    redirect,
    make_response,
    request,
    send_file,
    session,
)
from flask_nav import Nav
from flask_nav.elements import Navbar, View
from flask_bootstrap import Bootstrap
from flask_session import Session
from bots.bot import Bot, plot_drawdown, plot_so_sizes
from bots.edit_form import EditForm
import os


def create_app():
    return Flask(__name__, template_folder="templates")


default_bot = list([Bot(10, 25, 2.0, 1.05, 1.0, 30, "TA Standard")])

# Presets
presets = list(
    [
        Bot(11, 27.5, 2.0, 1.05, 1.0, 30, "TA Standard"),
        Bot(11, 11, 2.0, 1.09, 1.0, 30, "TA 2"),
        Bot(11, 11, 2.0, 1.4, 1.23, 10, "TA Strong Uptrend"),
        Bot(11, 11, 2.0, 1.03, 1.0, 30, "TA Budget"),
        Bot(11, 11, 1.84, 1.4, 1.5, 7, "Urma Lite v3"),
        Bot(11, 11, 1.8, 1.4, 1.3, 8, "Mars"),
        Bot(11, 11, 1.5, 1.4, 1.35, 9, "Banshee"),
        Bot(11, 11, 2.0, 1.4, 1.2, 10, "THEALPHA 3.0"),
        Bot(11, 11, 1.44, 1.33, 1.27, 10, "Scimitar"),
        Bot(11, 11, 1.81, 1.31, 1.28, 9, "Outrider"),
    ]
)

app = create_app()
app.config["SESSION_TYPE"] = "filesystem"
if os.getenv("SESSION_FILE_DIR", None) is not None:
    app.config["SESSION_FILE_DIR"] = os.getenv("SESSION_FILE_DIR")
app.secret_key = os.urandom(32)
Session(app)

Bootstrap(app)
nav = Nav()


@nav.navigation()
def navbar():
    return Navbar("3COMMAS DCA Analyzer", View("Bots", "index"), View("About", "about"))


nav.init_app(app)


@app.route("/add", methods=["GET", "POST"])
def add():
    """
    Add a new bot configuration.
    :return:
    """
    bots = session.get("bots", default_bot)
    form = EditForm(mode="add")
    if form.validate_on_submit():
        form.process_form(bots)
        session["bots"] = bots
        return redirect(url_for("index"))
    if request.args:
        form.populate_from_get(request)

    return render_template("edit.html", form=form, title="Add Bot")


@app.route("/<int:id>/edit", methods=["GET", "POST"])
def edit(id):
    """
    Change parameters of a bot configuration
    :param id: Index of bot in bots[] definition list
    :return:
    """
    bots = session.get("bots", default_bot)
    form = EditForm(obj=bots[id], mode="edit")
    if form.validate_on_submit():
        form.process_form(bots, id)
        session["bots"] = bots
        return redirect(url_for("index"))
    return render_template("edit.html", form=form, title="Edit Bot")


@app.route("/<int:id>/delete")
def delete(id):
    """
    Delete a bot configuration from list.
    :param id: Index of bot in bots[] definition list
    :return:
    """
    bots = session.get("bots", default_bot)
    del bots[id]
    session["bots"] = bots
    return redirect(url_for("index"))


@app.route("/", methods=["GET"])
def index():
    """
    Main view.
    :return:
    """
    bots = session.get("bots", default_bot)

    resp = make_response(
        render_template(
            "index.html",
            title="Welcome to 3COMMAS Grapher",
            bots=bots,
            presets=presets,
            plot_left=plot_drawdown(bots, html=True),
            plot_right=plot_so_sizes(bots, html=True),
        )
    )
    return resp


@app.route("/drawdown")
def drawdown_image():
    """
    Plot bots[] in a deviation vs. average prize chart to a single image file
    :return:
    """
    bots = session.get("bots", default_bot)
    d = plot_drawdown(bots, html=False)
    return send_file(d, mimetype="image/png")


@app.route("/so_sizes")
def show_so_sizes():
    """
    Plot bots[] in a deviation vs. order size chart to a single image file
    :return:
    """
    bots = session.get("bots", default_bot)
    d = plot_so_sizes(bots, html=False)
    return send_file(d, mimetype="image/png")


@app.route("/about")
def about():
    return render_template("about.html", title="About 3COMMAS Grapher")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))  # nosec
