import matplotlib.pyplot as plt
import io
import base64

base_prize = 100


class Bot(object):
    bo = 10.0
    so = 10.0
    sos = 2.0
    os = 1.01
    ss = 1.0
    mstc = 30
    name = ""
    steps = []

    def __init__(self, bo, so, sos, os, ss, mstc, name):
        self.bo = bo
        self.so = so
        self.sos = sos
        self.os = os
        self.ss = ss
        self.mstc = mstc
        self.name = name

    def fill_steps(self):
        if not self.steps:
            self.steps = [
                {
                    "deviation": 0,
                    "size": self.bo,
                    "prize": base_prize,
                    "quantity": self.bo / base_prize,
                }
            ]
            for i in range(1, self.mstc + 1):
                deviation = self.steps[i - 1]["deviation"] + self.sos * pow(
                    self.ss, i - 1
                )
                size_inc = self.so * pow(self.os, i - 1)
                size = self.steps[i - 1]["size"] + size_inc
                prize = base_prize * (1 - deviation / 100)

                quantity_inc = size_inc / prize
                quantity = self.steps[i - 1]["quantity"] + quantity_inc
                self.steps.append(
                    {"deviation": deviation, "size": size, "quantity": quantity}
                )

    def prize(self):
        total = 0
        for step in self.steps:
            total = +step["size"]
        return round(total)

    def deviation_avg_prize(self):
        self.fill_steps()
        avg_prize = [base_prize]
        deviation = [self.steps[0]["size"] / base_prize]
        for count, step in enumerate(self.steps):
            avg_prize.append(step["size"] / step["quantity"])
            deviation.append(step["deviation"])

        return deviation, avg_prize

    def deviation_so_size(self):
        self.fill_steps()
        sizes = []
        deviation = []
        for count, step in enumerate(self.steps):
            if count == 0:
                sizes.append(self.bo)
            else:
                sizes.append(self.so * pow(self.ss, count))
            deviation.append(step["deviation"])

        return deviation, sizes


def plot_drawdown(bots, html=True):
    fig, axs = plt.subplots(1, 1, figsize=(11, 8))
    for bot in bots:
        deviation, avg_prize = bot.deviation_avg_prize()
        axs.step(deviation, avg_prize, where="post", label=bot.name)
    axs.legend()
    plt.minorticks_on()
    plt.grid(axis="y", which="major")
    plt.grid(axis="y", which="minor", linestyle=":")
    plt.xlabel("Deviation [%]")
    plt.ylabel("Average prize")
    plt.title("Drawdown behavior")

    image_binary = io.BytesIO()

    fig.tight_layout()
    plt.savefig(image_binary, format="png")
    image_binary.seek(0)
    # image_binary.close()
    if html:
        data = base64.b64encode(image_binary.getbuffer()).decode("ascii")
        return f"""<div class='col-md-6'>
    <a href='/drawdown'>
        <img class='img-responsive plot' src='data:image/png;base64,{data}'/>
    </a>
</div>
"""

    else:
        return image_binary


def plot_so_sizes(bots, html=True):
    fig, axs = plt.subplots(
        1, 1, figsize=(11, 8)
    )  # figsize is the size of the plot created, change if needed
    for bot in bots:
        deviation, so_size = bot.deviation_so_size()
        axs.bar(deviation, so_size, label=bot.name, alpha=0.8, width=1)
        axs.plot(deviation, so_size)
    axs.legend()
    plt.minorticks_on()
    plt.grid(axis="y", which="major")
    plt.grid(axis="y", which="minor", linestyle=":")
    plt.xlabel("Deviation [%]")
    plt.ylabel("Order size")
    plt.title("Order sizes")

    image_binary = io.BytesIO()

    fig.tight_layout()
    plt.savefig(image_binary, format="png")
    image_binary.seek(0)
    # image_binary.close()
    if html:
        data = base64.b64encode(image_binary.getbuffer()).decode("ascii")
        return f"""<div class='col-md-6'>
    <a href='/so_sizes'>
        <img class='img-responsive plot' src='data:image/png;base64,{data}' alt='Plotted data'/>
    </a>
</div>
"""
    else:
        return image_binary
