from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, DecimalField, IntegerField
from wtforms.validators import DataRequired
from .bot import Bot


class EditForm(FlaskForm):
    name = StringField(
        "Name", validators=[DataRequired()], description="Plot legend label"
    )
    bo = DecimalField("BO", description="Base order size")
    so = DecimalField("SO", description="Safety order size")
    sos = DecimalField("SOS", description="Initial safety order deviation")
    os = DecimalField("OS", description="Safety order volume scale")
    ss = DecimalField("SS", description="Safety order step scale")
    mstc = IntegerField("MSTC", description="Safety order step scale")
    submit = SubmitField("Save bot")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Edit Bot"
        self.mode = kwargs["mode"]

    # def validate(self):
    #     rv = FlaskForm.validate(self)
    #     if not rv:  # pragma: no cover
    #         return False  # pragma: no cover
    #
    #     return True

    def process_form(self, bots, index=-1):
        bot = Bot(
            float(self.bo.data),
            float(self.so.data),
            float(self.sos.data),
            float(self.os.data),
            float(self.ss.data),
            int(self.mstc.data),
            self.name.data,
        )
        if self.mode == "add":
            bots.append(bot)
        elif self.mode == "edit":
            bots[index] = bot
            return bots

    def populate_from_get(self, request):
        self.name.data = request.args.get("name")
        self.bo.data = float(request.args.get("bo"))
        self.so.data = float(request.args.get("so"))
        self.sos.data = float(request.args.get("sos"))
        self.os.data = float(request.args.get("os"))
        self.ss.data = float(request.args.get("ss"))
        self.mstc.data = int(request.args.get("mstc"))
