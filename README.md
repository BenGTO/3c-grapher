# 3c-grapher

A tool to visualize the behavior of [3commas](https://3commas.io) DCA trading bots.

Plots the effect of increasing deviation on average price, and on order size. Choose from a series of bots from members of the
[Trade Alts discord](discord.gg/BCz3mVw)), or design your own bot.
