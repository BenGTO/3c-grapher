FROM python:3.9-slim
# ARG VCS_TAG

LABEL maintainer="Bengt Giger <beni@directbox.com>"
ENV PYTHONUNBUFFERED=1

COPY app /app
COPY requirements.txt /app/

ENV APP_HOME /app
WORKDIR $APP_HOME
RUN pip install -r requirements.txt
# RUN echo $VCS_TAG >.version.txt

RUN addgroup --system py && adduser --system --ingroup py py
USER py:py

ENV FLASK_APP graph3cs.py
ENV SESSION_FILE_DIR /tmp
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 main:app
